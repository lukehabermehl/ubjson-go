package ubjsongo

import (
    "testing"
)

const (
	tint int = 1234
	tint64 int64 = 1234567
	tfloat32 float32 = 12.345
	tfloat64 float64 = 12345.6789
	tbool = true
	tstring = "string"
	tbyte byte = 0xB
)

func assertEqual(v interface{}, c interface{}, t *testing.T) {
	if v != c {
		t.Errorf("Assert Equal failed: %v != %v", v, c)
	}
}

func assertValue(val *Value, kind Kind, raw interface{}, t *testing.T) {
	if val.kind != kind {
		t.Errorf("Assert Value failed: kind %s != %s", val.kind.String(), kind.String())
	}
	if val.value != raw {
		t.Errorf("Assert Value failed: value %v != %v", val.value, raw)
	}
}

func TestValueGetSet(t *testing.T) {
	val := new(Value)
	
	val.SetInt(tint)
	assertValue(val, kUBJInt32, int32(tint), t)
	i, err := val.GetInt()
	if err != nil {
		t.Error(err)
	}
	assertEqual(i, tint, t)
	
	val.SetInt64(tint64)
	assertValue(val, kUBJInt64, tint64, t)
	i64, err := val.GetInt64()
	if err != nil {
		t.Error(err)
	}
	assertEqual(i64, tint64, t)
	
	val.SetFloat32(tfloat32)
	assertValue(val, kUBJFloat32, tfloat32, t)
	f32, err := val.GetFloat32()
	if err != nil {
		t.Error(err)
	}
	assertEqual(f32, tfloat32, t)
	
	val.SetFloat64(tfloat64)
	assertValue(val, kUBJFloat64, tfloat64, t)
	f64, err := val.GetFloat64()
	if err != nil {
		t.Error(err)
	}
	assertEqual(f64, tfloat64, t)
	
	val.SetBool(tbool)
	assertValue(val, kUBJBool, tbool, t)
	b, err := val.GetBool()
	if err != nil {
		t.Error(err)
	}
	assertEqual(b, tbool, t)
	
	val.SetNull()
	assertValue(val, kUBJNull, nil, t)
	assertEqual(val.IsNull(), true, t)
	
	val.SetString(tstring)
	assertValue(val, kUBJString, tstring, t)
	s, err := val.GetString()
	if err != nil {
		t.Error(err)
	}
	assertEqual(s, tstring, t)
	
	val.SetByte(tbyte)
	assertValue(val, kUBJByte, tbyte, t)
	by, err := val.GetByte()
	if err != nil {
		t.Error(err)
	}
	assertEqual(by, tbyte, t)
}

func TestKindOf(t *testing.T) {
	var ts = struct {
		num int
	}{
		num: 10,
	}	
	assertEqual(KindOf(string("string")), kUBJString, t)
	assertEqual(KindOf(int8(10)), kUBJInt8, t)
	assertEqual(KindOf(int16(10)), kUBJInt16, t)
	assertEqual(KindOf(int32(100)), kUBJInt32, t)
	assertEqual(KindOf(int64(1000)), kUBJInt64, t)
	assertEqual(KindOf(uint8(10)), kUBJUInt8, t)
	assertEqual(KindOf(float32(10.10)), kUBJFloat32, t)
	assertEqual(KindOf(float64(100.10)), kUBJFloat64, t)
	assertEqual(KindOf(true), kUBJBool, t)
	assertEqual(KindOf([]string{"s"}), kUBJArray, t)
	assertEqual(KindOf(ts), kUBJObject, t)
}

func TestValueFor(t *testing.T) {
	var val *Value
	s := "string"
	val = ValueFor(s)
	assertValue(val, kUBJString, s, t)
	
	i8 := int8(10)
	val = ValueFor(i8)
	assertValue(val, kUBJInt8, i8, t)
	
	f64 := float64(10.10)
	val = ValueFor(f64)
	assertValue(val, kUBJFloat64, f64, t)
	
	b := true
	val = ValueFor(b)
	assertValue(val, kUBJBool, b, t)
}

func TestArrayFromSlice(t *testing.T) {
	slc := []string{"one", "two", "three"}
	arr := ArrayFromSlice(slc)
	for i:=0; i<len(arr); i++ {
		assertValue(arr[i], kUBJString, slc[i], t)
	}
}

