package ubjsongo

var ids = struct {
	NULL,
	NOOP,
	TRUE,
	FALSE,
	BYTE,
	INT8,
	UINT8,
	INT16,
	INT32,
	INT64,
	FLOAT32,
	FLOAT64,
	HUGE,
	STRING,
	ARRAY,
	ARRAY_LONG,
	ARRAY_OPEN,
	ARRAY_CLOSE,
	OBJECT,
	OBJECT_LONG,
	OBJECT_OPEN,
	OBJECT_CLOSE,
	CONTAINER_CLOSE byte
} {
	NULL:				byte('Z'),
	NOOP:				byte('N'),
	TRUE:				byte('T'),
	FALSE:				byte('F'),
	BYTE:				byte('B'),
	INT8:				byte('i'),
	UINT8:				byte('U'),
	INT16:				byte('I'),
	INT32:				byte('l'),
	INT64:				byte('L'),
	FLOAT32:			byte('d'),
	FLOAT64:			byte('D'),
	HUGE:				byte('H'),
	STRING:				byte('S'),
	ARRAY:				byte('a'),
	ARRAY_LONG:			byte('A'),
	ARRAY_OPEN:			byte('['),
	ARRAY_CLOSE:		byte(']'),
	OBJECT:				byte('o'),
	OBJECT_LONG:		byte('O'),
	OBJECT_OPEN:		byte('{'),
	OBJECT_CLOSE:		byte('}'),
	CONTAINER_CLOSE:	byte('E'),
}

//Returns the number of bytes for each ubjson type (including identifier byte)
func SizeForType(identifier byte) int {
	switch identifier {
		case ids.NULL, ids.NOOP, ids.TRUE, ids.FALSE:
			return 1
			
		case ids.BYTE, ids.HUGE, ids.INT8,
				ids.ARRAY, ids.ARRAY_OPEN, ids.OBJECT, ids.OBJECT_OPEN:
			return 2
			
		case ids.INT16, ids.STRING:
			return 3
			
		case ids.INT32, ids.FLOAT32, ids.ARRAY_LONG, ids.OBJECT_LONG:
			return 5
			
		case ids.INT64, ids.FLOAT64:
			return 9
		
		default:
			return -1		
	}
}

func isNumberType(identifier byte) bool {
	tlist := []byte{ids.INT8, ids.UINT8, ids.INT16, ids.INT32,
					ids.INT64, ids.FLOAT32, ids.FLOAT64}
	
	for _, b := range tlist {
		if b == identifier {
			return true
		}
	}
	
	return false				
}

