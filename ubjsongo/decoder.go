package ubjsongo

import (
	"bytes"
	"fmt"
	"encoding/binary"
)

func decodeInt8 (data []byte) (int8, int32, error) {
	lengthOffset := 1
	var i8 int8
	buf := bytes.NewReader(data[0:lengthOffset])
	err := binary.Read(buf, binary.BigEndian, &i8)
	
	return i8, int32(lengthOffset), err
}

func decodeInt32 (data []byte) (int32, int32, error) {
	lengthOffset := 4
	var i32 int32
	buf := bytes.NewReader(data[0:lengthOffset])
	err := binary.Read(buf, binary.BigEndian, &i32)
	
	return i32, int32(lengthOffset), err
}

func decodeNumber(data []byte) (interface{}, int32, error) {
	bytelength := SizeForType(data[0])
	if (bytelength < 0) {
		return nil, 0, NewParseError("Invalid type identifier while decoding number")
	}
	
	buf := bytes.NewReader(data[1:bytelength])
	var err error
	var value interface{}
	
	switch data[0] {
		case ids.INT8:
			var v int8
			err = binary.Read(buf, binary.BigEndian, &v)
			value = v
			
		case ids.INT16:
			var v int16
			err = binary.Read(buf, binary.BigEndian, &v)
			value = v	
		
		case ids.INT32:
			var v int32
			err = binary.Read(buf, binary.BigEndian, &v)
			value = v
			
		case ids.INT64:
			var v int32
			err = binary.Read(buf, binary.BigEndian, &v)
			value = v
			
		case ids.FLOAT32:
			var v float32
			err = binary.Read(buf, binary.BigEndian, &v)
			value = v
			
		case ids.FLOAT64:
			var v float64
			err = binary.Read(buf, binary.BigEndian, &v)
			value = v
			
		default:
			return nil, 0, NewParseError("Found non-number type while decoding number")						
	}
	
	return value, int32(bytelength), err
}

func decodeString(data []byte) (string, int32, error) {
	var length int64
	var err error
	var lengthOffset int64
	
	switch data[0] {
		case ids.STRING: {
			l, o, e := decodeNumber(data[1:])
			if (e == nil) {
				switch data[1] {
					case ids.INT8:
						length = int64(l.(int8))
					case ids.UINT8:
						length = int64(l.(uint8))
					case ids.INT16:
						length = int64(l.(int16))
					case ids.INT32:
						length = int64(l.(int32))
					case ids.INT64:
						length = l.(int64)
				}
				lengthOffset = 1+int64(o)
			} else {
				err = e
			}
		}
			
		case ids.INT8:
			l, o, e := decodeNumber(data)
			if (e == nil) {
				length = int64(l.(int8))
				lengthOffset = int64(o)
			} else {
				err = e
			}
			
		case ids.INT32:
			l, o, e := decodeNumber(data)
			if (e == nil) {
				length = int64(l.(int32))
				lengthOffset = int64(o)
			} else {
				err = e
			}
		
		default:
			errStr := fmt.Sprintf("Invalid identifier found while decoding string: %x", data[0])
			return "", 0, NewParseError(errStr)		
	}
	
	if (err != nil) {
		return "", 0, err
	}
	
	totalOffset := lengthOffset + length
	stringBytes := data[lengthOffset:totalOffset]
	return string(stringBytes), int32(totalOffset), nil
}

func decodeValue(data []byte) (*Value, int32, error) {
	typec := data[0]
	offset := int32(SizeForType(typec))
	switch typec {
		case ids.NULL:
			return newValue(nil, kUBJNull), offset, nil
		
		case ids.NOOP:
			return newValue(nil, kUBJNOOP), offset, nil	
			
		case ids.TRUE:
			return newValue(true, kUBJBool), offset, nil
			
		case ids.FALSE:
			return newValue(false, kUBJBool), offset, nil
		
		case ids.BYTE:
			return newValue(data[1], kUBJByte), offset, nil
			
		case ids.ARRAY, ids.ARRAY_LONG, ids.ARRAY_OPEN:
			arr, o, err := decodeArray(data)
			return newValue(arr, kUBJArray), o, err
		
		case ids.OBJECT, ids.OBJECT_LONG, ids.OBJECT_OPEN:
			obj, o, err := decodeObject(data)
			return newValue(obj, kUBJObject), o, err
		
		case ids.STRING, ids.HUGE:
			s, o, err := decodeString(data)
			var typeid Kind
			if (typec == ids.HUGE) {
				typeid = kUBJHuge
			} else {
				typeid = kUBJString
			}
			return newValue(s, typeid), o, err
	}
	
	if isNumberType(typec) {
		v, o, err := decodeNumber(data)
		var typeid Kind
		switch typec {
			case ids.INT8:
				typeid = kUBJInt8
			case ids.UINT8:
				typeid = kUBJUInt8	
			case ids.INT16:
				typeid = kUBJInt16
			case ids.INT32:
				typeid = kUBJInt32
			case ids.INT64:
				typeid = kUBJInt64
			case ids.FLOAT32:
				typeid = kUBJFloat32
			case ids.FLOAT64:
				typeid = kUBJFloat64						
		}
		return newValue(v, typeid), o, err
	}
	
	errStr := fmt.Sprintf("Invalid identifier found while decoding value: %c", typec)
	err := NewParseError(errStr)
	return nil, 0, err
}

func decodeArray(data []byte) (Array, int32, error) {
	var length int32
	var lengthOffset int32
	lengthIsKnown := true
	
	switch data[0] {
		case ids.ARRAY:
			l, o, err := decodeInt8(data[1:])
			if (err != nil) {
				return nil, 0, err
			}
			
			if (length == 255) {
				lengthIsKnown = false
			}
			
			length = int32(l)
			lengthOffset = 1+o
			
		case ids.ARRAY_OPEN:
			length = 2;
			lengthIsKnown = false;
			lengthOffset = 1	
			
		case ids.ARRAY_LONG:
			l, o, err := decodeInt32(data[1:])
			if (err != nil) {
				return nil, 0, err
			}
			
			length = l
			lengthOffset = 1+o
			
		default:
			return nil, 0, NewParseError("Invalid identifier while decoding array")		
	}
	
	var totalOffset int32 = lengthOffset
	var count int32 = 0
	var arr []*Value
	
	if lengthIsKnown {
		arr = make([]*Value, 0, length)
	} else {
		arr = make([]*Value, 0, 10)
	}
	
	for count < length {
		innerdata := data[totalOffset:]
		if (!lengthIsKnown) {
			if (innerdata[0] == ids.CONTAINER_CLOSE || innerdata[0] == ids.ARRAY_CLOSE) {
				break
			} else {
				count = 0
			}
		}
		
		v, o, err := decodeValue(innerdata)
		if (err != nil) {
			return arr, totalOffset, err
		}
		
		arr = append(arr, v)
		totalOffset += (o)
		count++
	}
	
	return Array(arr), totalOffset, nil
}

func decodeObject(data []byte) (Object, int32, error) {
	var length int32
	var lengthOffset int32
	var err error = nil
	lengthIsKnown := true
	
	switch data[0] {
		case ids.OBJECT:
			l, o, e := decodeInt8(data[1:])
			if (e == nil) {
				if (length == 255) {
					lengthIsKnown = false
				}
				length = int32(l)
				lengthOffset = 1+o
			} else {
				err = e
			}
		
		case ids.OBJECT_LONG:
			l, o, e := decodeInt32(data[1:])
			if (e == nil) {
				length = l
				lengthOffset = 1+o
			} else {
				err = e
			}
			
		case ids.OBJECT_OPEN:
			length = 255
			lengthOffset = 1
			lengthIsKnown = false	
		
		default:
			return nil, 0, NewParseError("Invalid identifier found while decoding object")		
			
	}
	
	if (err != nil) {
		return nil, 0, err
	}
	
	obj := MakeObject()
	var count int32 = 0
	var totalOffset int32 = lengthOffset
	for count < length {
		innerData := data[totalOffset:]
		//Handle unspecified length
		if (!lengthIsKnown) {
			if (innerData[0] == ids.CONTAINER_CLOSE || innerData[0] == ids.OBJECT_CLOSE) {
				break
			} else {
				count = 0
			}
		}
		
		key, o, err := decodeString(innerData)
		if (err != nil) {
			return obj, totalOffset, err
		}
		
		totalOffset += o
		innerData = data[totalOffset:]
		
		v, o, err := decodeValue(innerData)
		if (err != nil) {
			return obj, totalOffset, err
		}
		
		totalOffset += o
		obj[key] = v
		count++
	}
	
	return obj, totalOffset, nil
}

func Decode(data []byte) (*Value, error) {
	val, _, err := decodeValue(data)
	return val, err
}