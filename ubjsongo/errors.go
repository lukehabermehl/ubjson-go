package ubjsongo

import (
	"fmt"
	"reflect"
)	

type ParseError struct {
	reason string
}

func (err ParseError) Error() string {
	return fmt.Sprintf("UBJSON parse error: %s", err.reason)
}

func NewParseError(reason string) *ParseError {
	e := new(ParseError)
	e.reason = reason
	return e
}

type TypeCastError struct {
	attempted string
	actual Kind
}

func (err TypeCastError) Error() string {
	return fmt.Sprintf("Could not cast type %q to %s", err.actual, err.attempted)
}

func newTypeCastError(attempt string, ubjtype Kind) *TypeCastError {
	e := new(TypeCastError)
	e.attempted = attempt
	e.actual = ubjtype
	
	return e
}

type EncodeError struct {
	attempted string
	actual Kind
}

func (err EncodeError) Error() string {
	return fmt.Sprintf("UBJSON Encode Error: Could not encode type %s as %s", err.actual.String(), err.attempted)
}

func newEncodeError(attempted string, actual Kind) *EncodeError {
	e := new(EncodeError)
	e.attempted = attempted
	e.actual = actual
	return e
}

type MarshalError struct {
	reason string
}

func (err MarshalError) Error() string {
	return err.reason
}

func newMarshalError(kind reflect.Kind) *MarshalError {
	e := new(MarshalError)
	e.reason = fmt.Sprintf("Marhsal encountered incompatible kind: %v", kind.String())
	return e
}

type UnmarshalError struct {
	reason string
}

func (err UnmarshalError) Error() string {
	return err.reason
}

func newUnmarshalError(fieldName string, kind reflect.Kind, ubjtype Kind) *UnmarshalError {
	e := new(UnmarshalError)
	e.reason = fmt.Sprintf("Could not unmarshal field: %s. Expected type: %v got %v", fieldName, ubjtype.String(), kind.String())
	return e
}
