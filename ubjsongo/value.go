package ubjsongo

import (
	"reflect"
)

type Kind uint
const (
	kUBJInvalid Kind = iota
	kUBJInt8
	kUBJUInt8
	kUBJInt16
	kUBJInt32
	kUBJInt64
	kUBJFloat32
	kUBJFloat64
	kUBJNull
	kUBJString
	kUBJObject
	kUBJArray
	kUBJHuge
	kUBJBool
	kUBJByte
	kUBJNOOP
)

func KindOf(i interface {}) Kind {
	if i == nil { return kUBJNull }
	
	rval := reflect.ValueOf(i)
	switch rval.Kind() {
		case reflect.Int8:
			return kUBJInt8
		case reflect.Int16:
			return kUBJInt16
		case reflect.Int32, reflect.Int:
			return kUBJInt32
		case reflect.Int64:
			return kUBJInt64
		case reflect.Float32:
			return kUBJFloat32
		case reflect.Float64:
			return kUBJFloat64
		case reflect.String:
			return kUBJString	
		case reflect.Slice:
			return kUBJArray
		case reflect.Struct:
			return kUBJObject
		case reflect.Bool:
			return kUBJBool
		case reflect.Uint8:
			return kUBJUInt8									
	}
	
	return kUBJInvalid
}

func (k Kind) String() string {
	switch k {
		case kUBJInt8:
			return "int8"
		case kUBJUInt8:
			return "uint8"	
		case kUBJInt16:
			return "int16"
		case kUBJInt32:
			return "int32"
		case kUBJInt64:
			return "int64"
		case kUBJFloat32:
			return "float32"
		case kUBJFloat64:
			return "float64"	
		case kUBJNull:
			return "null"
		case kUBJString:
			return "string"
		case kUBJObject:
			return "object"
		case kUBJArray:
			return "array"
		case kUBJHuge:
			return "huge"
		case kUBJBool:
			return "bool"
		case kUBJByte:
			return "byte"												
	}
	return "Invalid"
}

type Value struct {
	value interface{}
	kind Kind
}

func NewValue() *Value {
	return newValue(nil, kUBJNull)
}

func (value *Value) RawValue() interface{} {
	return value.value
}

func (value *Value) Kind() Kind {
	return value.kind
}

func (value *Value) GetObject() (Object, error) {
	if value.kind == kUBJObject {
		return value.value.(Object), nil
	}
	
	return nil, newTypeCastError("Object", value.kind)
}

func (value *Value) GetArray() (Array, error) {
	if value.kind == kUBJArray {
		return value.value.(Array), nil
	}
	
	return nil, newTypeCastError("Array", value.kind)
}

func (value *Value) GetString() (string, error) {
	if (value.kind == kUBJString) {
		return value.value.(string), nil
	}
	
	return "", newTypeCastError("String", value.kind)
}

func (value Value) GetBool() (bool, error) {
	if (value.kind == kUBJBool) {
		return value.value.(bool), nil
	}
	
	return false, newTypeCastError("Bool", value.kind)
}

func (value *Value) GetInt() (int, error) {
	switch value.kind {
		case kUBJInt8:
			i := value.value.(int8)
			return int(i), nil
		
		case kUBJUInt8:
			i := value.value.(uint8)
			return int(i), nil	
		
		case kUBJInt16:
			i := value.value.(int16)
			return int(i), nil
		
		case kUBJInt32:
			i := int(value.value.(int32))
			return int(i), nil	
		
		default:
			return 0, newTypeCastError("Int", value.kind)		
	}
}

func (value *Value) GetInt64() (int64, error) {
	switch value.kind {
		case kUBJInt8:
			i := value.value.(int8)
			return int64(i), nil
		
		case kUBJInt16:
			i := value.value.(int16)
			return int64(i), nil
		
		case kUBJInt32:
			i := value.value.(int32)
			return int64(i), nil
		
		case kUBJInt64:
			i := value.value.(int64)
			return i, nil		
		
		default:
			return 0, newTypeCastError("Int64", value.kind)		
	}
}

func (value *Value) GetFloat32() (float32, error) {
	switch value.kind {
		case kUBJInt8, kUBJInt16, kUBJInt32:
			i, err := value.GetInt64()
			if (err != nil) {
				return 0, err
			}
			
			return float32(i), nil
		
		case kUBJFloat32:
			return value.value.(float32), nil
			
		default:
			return 0, newTypeCastError("Float32", value.kind)		
	}
}

func (value *Value) GetFloat64() (float64, error) {
	switch value.kind {
		case kUBJInt8, kUBJInt16, kUBJInt32, kUBJInt64:
			i, err := value.GetInt64()
			if (err != nil) {
				return 0, err
			}
			
			return float64(i), nil
			
		case kUBJFloat32:
			f := value.value.(float32)
			return float64(f), nil
		
		case kUBJFloat64:
			return value.value.(float64), nil
		
		default:
			return 0, newTypeCastError("Float64", value.kind)			
	}
}

func (value *Value) GetByte() (byte, error) {
	if value.kind == kUBJInt8 || value.kind == kUBJByte {
		return value.value.(byte), nil
	}
	
	return 0x0, newTypeCastError("Byte", value.kind)
}

func (value *Value) IsNull() bool {
	if value.kind == kUBJNull {
		return true
	}
	
	return false
}

func (value *Value) SetObject(obj Object) {
	value.value = obj
	value.kind = kUBJObject
}

func (value *Value) SetArray(arr Array) {
	value.value = arr
	value.kind = kUBJArray
}

func (value *Value) SetString(s string) {
	value.value = s
	value.kind = kUBJString
}

func (value *Value) SetBool(b bool) {
	value.value = b
	value.kind = kUBJBool
}

func (value *Value) SetInt(n int) {
	value.value = int32(n)
	value.kind = kUBJInt32
}

func (value *Value) SetInt64(n int64) {
	value.value = n
	value.kind = kUBJInt64
}

func (value *Value) SetFloat32(f float32) {
	value.value = f
	value.kind = kUBJFloat32
}

func (value *Value) SetFloat64(f float64) {
	value.value = f
	value.kind = kUBJFloat64
}

func (value *Value) SetByte(b byte) {
	value.value = b
	value.kind = kUBJByte
}

func (value *Value) SetNull() {
	value.value = nil
	value.kind = kUBJNull
}

func (value *Value) SetValue(v *Value) {
	value.value = v.value
	value.kind = v.kind
}

func ValueFor(i interface{}) *Value {
	k := KindOf(i)
	switch k {
		case kUBJObject:
			obj, err := objectFromStruct(i)
			if err != nil {
				return newValue(i, kUBJInvalid)
			}
			return newValue(obj, k)
		case kUBJArray:
			//TODO make array		
	}
	
	return newValue(i, k)
}

func newValue(value interface{}, kind Kind) *Value {
	v := new(Value)
	v.value = value
	v.kind = kind
	
	return v
}

type Object map[string]*Value

func (obj Object) NewValueWithKey(name string) *Value {
	//TODO check if key already exists
	obj[name] = newValue(nil, kUBJNull)
	return obj[name]
}

func (obj Object) Value() *Value {
	v := newValue(obj, kUBJObject)
	return v
}

func MakeObject() Object {
	obj := make(map[string]*Value)
	return obj
}

type Array []*Value

func ArrayWithLength(length int) Array {
	arr := make([]*Value, length)
	for i:=0; i<length; i++ {
		arr[i] = NewValue()
	}
	
	return arr
}

func ArrayFromSlice(slice interface{}) Array {
	rval := reflect.ValueOf(slice)
	if rval.Kind() != reflect.Slice {
		panic("ArrayFromSlice() given non-slice interface")
	}
	
	length := rval.Len()
	arr := make([]*Value, length)
	k := KindOf(rval.Index(0).Interface())
	for i:=0; i<length; i++ {
		arr[i] = newValue(rval.Index(i).Interface(), k)
	}
	
	return arr
}

