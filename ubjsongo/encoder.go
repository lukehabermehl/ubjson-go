package ubjsongo

import  (
	"bytes"
	"encoding/binary"
)

func encodeNumber(val *Value) ([]byte, error) {
	var typec byte
	switch val.kind {
		case kUBJInt8:
			typec = ids.INT8
		case kUBJUInt8:
			typec = ids.UINT8	
		case kUBJInt16:
			typec = ids.INT16
		case kUBJInt32:
			typec = ids.INT32
		case kUBJInt64:
			typec = ids.INT64
		case kUBJFloat32:
			typec = ids.FLOAT32
		case kUBJFloat64:
			typec = ids.FLOAT64
		default:
			return nil, newEncodeError("Number", val.kind)						
	}
	
	length := SizeForType(typec)
	data := make([]byte, 1, length)
	data[0] = typec
	
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.BigEndian, val.value)
	
	if (err != nil) {
		return nil, err
	}
	
	data = append(data, buf.Bytes()...)
	return data, nil
}

func encodeBool (val *Value) ([]byte, error) {
	b, err := val.GetBool()
	if err != nil {
		return nil, newEncodeError("Bool", val.kind)
	}
	
	data := make([]byte, 1)
	
	if b {
		data[0] = ids.TRUE
	} else {
		data [0] = ids.FALSE
	}
	
	return data, nil
}

func encodeString(val *Value) ([]byte, error) {
	s, err := val.GetString()
	if err != nil {
		return nil, newEncodeError("String", val.kind)
	}
	sbytes := []byte(string(s))
	strLength := len(sbytes)
	typec := ids.STRING
	
	length := SizeForType(typec) + strLength;
	data := make([]byte, 1, length)
	buf := new(bytes.Buffer)
	data[0] = typec
	
	if strLength > 127 {
		data = append(data, byte('l'))
		binary.Write(buf, binary.BigEndian, int32(strLength))
	} else {
		data = append(data, byte('i'))
		binary.Write(buf, binary.BigEndian, int8(strLength))
	}
	
	data = append(data, buf.Bytes()...)
	data = append(data, sbytes...)
	
	return data, nil
}

func encodeByte(val *Value) ([]byte, error) {
	b, err := val.GetByte()
	
	if (err != nil) {
		return nil, newEncodeError("Byte", val.kind)
	}
	
	typec := ids.BYTE
	data := []byte{typec, b}
	
	return data, nil
}

func encodeArray(val *Value) ([]byte, error) {
	arr, err := val.GetArray()
	if (err != nil) {
		return nil, newEncodeError("Array", val.kind)
	}
	
	var typec byte
	arrayLength := len(arr)
	
	if arrayLength >= 255 {
		typec = ids.ARRAY_LONG
	} else {
		typec = ids.ARRAY
	}
	
	data := make([]byte, 1, SizeForType(typec) + arrayLength)
	data[0] = typec
	
	buf := new(bytes.Buffer)
	
	switch typec {
		case ids.ARRAY:
			binary.Write(buf, binary.BigEndian, int8(arrayLength))
		case ids.ARRAY_LONG:
			binary.Write(buf, binary.BigEndian, int32(arrayLength))	
	}
	
	data = append(data, buf.Bytes()...)
	
	for _, item := range arr {
		itemData, err := EncodeValue(item)
		if (err != nil) {
			return data, err
		}
		
		data = append(data, itemData...)
	} 
	
	return data, nil
}

func encodeObject(val *Value) ([]byte, error) {
	obj, err := val.GetObject()
	if (err != nil) {
		return nil, newEncodeError("Object", val.kind)
	}
	
	typec := ids.OBJECT_OPEN
	objLength := len(obj)
	
	data := make([]byte, 1, (2 * objLength) + 2)
	data[0] = typec
	
	for key, item := range obj {
		keyData, err := encodeString(newValue(key, kUBJString))
		if (err != nil) {
			return data, err
		}
		
		data = append(data, keyData...)
		
		itemData, err := EncodeValue(item)
		if (err != nil) {
			return data, err
		}
		
		data = append(data, itemData...)
	}
	
	data = append(data, ids.OBJECT_CLOSE)
	
	return data, nil
}

func EncodeValue(val *Value) ([]byte, error) {
	var data []byte
	var err error = nil
	
	switch val.kind {
		case kUBJInt8, kUBJInt16, kUBJInt32, kUBJInt64, kUBJFloat32, kUBJFloat64:
			data, err = encodeNumber(val)
		
		case kUBJBool:
			data, err = encodeBool(val)
		
		case kUBJString:
			data, err = encodeString(val)
		
		case kUBJNull:
			data = []byte{ids.NULL}
		
		case kUBJObject:
			data, err = encodeObject(val)
			
		case kUBJArray:
			data, err = encodeArray(val)
			
		case kUBJByte:
			data, err = encodeByte(val)			
			
		default:
			return nil, newEncodeError("Value", val.kind) 				
	}
	
	return data, err
}

func EncodeObject(obj Object) ([]byte, error) {
	val := newValue(obj, kUBJObject)
	data, err := EncodeValue(val)
	return data, err
}
