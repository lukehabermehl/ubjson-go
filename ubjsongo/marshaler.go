package ubjsongo

import (
	"reflect"
)

type Marshaler interface {
	MarshalUBJSON() ([]byte, error)
}

func arrayFromReflectValue(rval reflect.Value)(*Value, error) {
	arr := make([]*Value, rval.Len())
	var err error
	
	for i:=0; i<rval.Len(); i++ {
		val := rval.Index(i)
		arr[i], err = valueFromReflectValue(val)
		if err != nil {
			return nil, err
		}
	}
	
	return newValue(Array(arr), kUBJArray), nil
}

func valueFromReflectValue(rval reflect.Value) (*Value, error) {
	var ubjval *Value
	var err error
	
	switch rval.Kind() {
		case reflect.Bool:
			ubjval = newValue(rval.Bool(), kUBJBool)
		
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32:
			ubjval = newValue(int32(rval.Int()), kUBJInt32)
		
		case reflect.Int64:
			ubjval = newValue(rval.Int(), kUBJInt64)
		
		case reflect.Float32:
			ubjval = newValue(rval.Interface().(float32), kUBJFloat32)
		
		case reflect.Float64:
			ubjval = newValue(rval.Interface().(float64), kUBJFloat64)
		
		case reflect.String:
			ubjval = newValue(rval.String(), kUBJString)
			
		case reflect.Slice:
			ubjval, err = arrayFromReflectValue(rval)
		
		case reflect.Struct:
			var obj Object
			obj, err = objectFromStruct(rval.Interface())
			ubjval = newValue(obj, kUBJObject)
			
		default:
			ubjval = nil
			err = newMarshalError(rval.Kind())
	}
	
	return ubjval, err
}

func objectFromStruct(v interface{}) (Object, error) {
	obj := MakeObject()
	
	rval := reflect.ValueOf(v)
	numFields := rval.NumField()
	typeOfVal := rval.Type()
	for i:=0; i<numFields; i++ {
		s := rval.Field(i)
		keyName := typeOfVal.Field(i).Name
		ubjval, err := valueFromReflectValue(s)
		
		if (err != nil) {
			return nil, err
		}
		
		obj.NewValueWithKey(keyName).SetValue(ubjval)
	}
	
	return obj, nil
}

func Marshal(v interface{}) ([]byte, error) {
	if m, ok := v.(Marshaler); ok {
		data, err := m.MarshalUBJSON()
		return data, err
	}
	
	obj, err := objectFromStruct(v)
	if err != nil {
		return nil, err
	}
	
	data, err := EncodeObject(obj)
	return data, err
}
