package ubjsongo

import (
    "testing"
)

func testEncodeDecode(t *testing.T) {
	obj := MakeObject()
	stringArray := make([]*Value, 3)
	for i:=0; i<3; i++ {
		stringArray[i] = new(Value)
	}
	stringArray[0].SetString("one")
	stringArray[1].SetString("two")
	stringArray[2].SetString("three")
	
	obj.NewValueWithKey("Num").SetInt(1234)
	obj.NewValueWithKey("Str").SetString("string")
	obj.NewValueWithKey("Truth").SetBool(true)
	obj.NewValueWithKey("Lie").SetBool(false)
	obj.NewValueWithKey("Array").SetArray(Array(stringArray))
	obj.NewValueWithKey("Byte").SetByte(0xB)
	obj.NewValueWithKey("Float").SetFloat64(12.345)
	
	data, err := EncodeObject(obj)
	if err != nil {
		t.Error(err)
	}
	
	dval, err := Decode(data)
	if err != nil {
		t.Error(err)
	}
	
	dobj, err := dval.GetObject()
	if err != nil {
		t.Error(err)
	}
	for key, val := range dobj {
		if val.value != obj[key].value {
			t.Errorf("Decode mismatch for key: %s: %v != %v", key, val, obj[key])
		}
	}
}

