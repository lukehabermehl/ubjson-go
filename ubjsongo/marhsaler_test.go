package ubjsongo

import (
    "testing"
)

func failIfError(err error, t *testing.T) {
	if err != nil {
		t.Error(err.Error())
	}
}

func TestMarshal(t *testing.T) {
	type TestStruct struct {
		Num int
		Str string
		Truth bool
		Lie bool
		Numbers []string
	}
	
	ts := TestStruct{1234, "string", true, false, []string{"one", "two", "three"}}
	data, err := Marshal(ts)
	if (err != nil) {
		t.Error(err.Error())
	}
	
	dval, err := Decode(data)
	failIfError(err, t)
	
	obj, _ := dval.GetObject();
	Num, err := obj["Num"].GetInt()
	failIfError(err, t)
	
	Str, err := obj["Str"].GetString()
	failIfError(err, t)
	
	Numbers, _ := obj["Numbers"].GetArray()
	
	if Num != ts.Num {
		t.Errorf("Field Num: %v != %v", Num, ts.Num)
	}
	
	if Str != ts.Str {
		t.Errorf("Field Str: %v != %v", Str, ts.Str)
	}
	
	for i, v := range Numbers {
		s, _ := v.GetString()
		if s != ts.Numbers[i] {
			t.Errorf("Array mismatch index: %v :: %v != %v", i, s, ts.Numbers[i])
		}
	}
	
}

func TestUnmarshaler(t *testing.T) {
	type TestStruct struct {
		Num int
		Str string
		Truth bool
		Lie bool
		Numbers []string
	}
	ts := TestStruct{1234, "string", false, true, []string{"one", "two", "three"}}
	
	obj := MakeObject()
	obj.NewValueWithKey("Num").SetInt(int(5678))
	obj.NewValueWithKey("Str").SetString("hello")
	obj.NewValueWithKey("Truth").SetBool(true)
	obj.NewValueWithKey("Lie").SetBool(false)
	arr := make([]*Value, 3)
	arr[0] = newValue("three", kUBJString)
	arr[1] = newValue("four", kUBJString)
	arr[2] = newValue("five", kUBJString)
	obj.NewValueWithKey("Numbers").SetArray(Array(arr))
	
	data, err := EncodeObject(obj)
	failIfError(err, t)
	
	err = Unmarshal(&ts, data)
	failIfError(err, t)
	
	Num, _ := obj["Num"].GetInt()
	Str, _ := obj["Str"].GetString()
	Truth, _ := obj["Truth"].GetBool()
	Lie, _ := obj["Lie"].GetBool()
	if ts.Num != Num {
		t.Errorf("Field Num: %v != %v", ts.Num, Num)
	}
	if ts.Str != Str {
		t.Errorf("Field Str: %v != %v", ts.Str, Str)
	}
	if ts.Truth != Truth {
		t.Errorf("Field Truth: %v != %v", ts.Truth, Truth)
	}
	if ts.Lie != Lie {
		t.Errorf("Field Lie: %v != %v", ts.Lie, Lie)
	}
	correct := []string{"three", "four", "five"}
	for i, v := range ts.Numbers {
		if correct[i] != v {
			t.Errorf("Array mismatch index: %v:: %v != %v", i, v, correct[i])
		}		
	}
}

func TestSubStruct(t *testing.T) {
	type TestSubStruct struct {
		Num int
	}
	
	type TestStruct struct {
		Sub TestSubStruct
	}
	
	tss := TestSubStruct{1234}
	ts := TestStruct{tss}
	
	data, err := Marshal(ts)
	failIfError(err, t)
	
	dval, err := Decode(data)
	failIfError(err, t)
	
	obj, err := dval.GetObject()
	failIfError(err, t)
	
	subobj, err := obj["Sub"].GetObject()
	failIfError(err, t)
	
	Num, _ := subobj["Num"].GetInt()
	if Num != ts.Sub.Num {
		t.Errorf("Field Num: %v != %v", Num, ts.Sub.Num)
	}
	
	var ts2 TestStruct
	ts2.Sub = TestSubStruct{0}
	
	err = Unmarshal(&ts2, data)
	failIfError(err, t)
	
	if ts2.Sub.Num != ts.Sub.Num {
		t.Errorf("Unmarshal failed: %v != %v", ts2.Sub.Num, ts.Sub.Num)
	}
}

