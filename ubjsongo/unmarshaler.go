package ubjsongo

import (
	"fmt"
	"reflect"
)

type Unmarshaler interface {
	UnmarshalUBJSON([]byte) error
}

func indexOfFieldInStruct(name string, s reflect.Value) int {
	for i:=0; i<s.NumField(); i++ {
		if s.Type().Field(i).Name == name {
			return i
		}
	}
	
	return -1
}

func unmarshalValueToStruct(key string, ubjval *Value, st interface{}) error {
	rval := reflect.ValueOf(st)
	var copyVal reflect.Value
	
	
	if (rval.Kind() != reflect.Ptr) {
		rval = reflect.ValueOf(&st).Elem()
	} else {
		rval = reflect.Indirect(rval)
	}
	
	if (rval.Kind() == reflect.Interface) {
		copyVal = reflect.New(rval.Elem().Type()).Elem()
	} else {
		copyVal = rval
	}
	
	indx := indexOfFieldInStruct(key, copyVal)
	if indx < 0 {
		err := new(UnmarshalError)
		err.reason = fmt.Sprintf("Unmarshal could not find key: %s", key)
		return err
	}
	
	field := copyVal.Field(indx)
	fieldKind := field.Kind()
	switch ubjval.kind {
		case kUBJInt8, kUBJInt16, kUBJInt32:
			if fieldKind != reflect.Int && fieldKind != reflect.Int32 && fieldKind != reflect.Int64 {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}
			i, err := ubjval.GetInt()
			if err != nil {
				return err
			}
			v := reflect.ValueOf(i)
			field.Set(v)
			
		case kUBJInt64:
			if (fieldKind != reflect.Int64) {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}
			i, err := ubjval.GetInt64()
			if err != nil {
				return err
			}	
			field.SetInt(i)
		
		case kUBJFloat32:
			if fieldKind != reflect.Float32 {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}
			f, err := ubjval.GetFloat32()
			if err != nil {
				return err
			}
			v := reflect.ValueOf(f)
			field.Set(v)
		
		case kUBJFloat64:
			if fieldKind != reflect.Float64 {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}
			f, err := ubjval.GetFloat64()
			if err != nil {
				return err
			}	
			field.SetFloat(f)
		
		case kUBJString:
			if fieldKind != reflect.String {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}	
			s, err := ubjval.GetString()
			if err != nil {
				return err
			}
			field.SetString(s)
		
		case kUBJBool:
			if fieldKind != reflect.Bool {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}	
			b, err := ubjval.GetBool()
			if err != nil {
				return err
			}
			field.SetBool(b)
		
		case kUBJByte:
			if fieldKind != reflect.Uint8 {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}	
			b, err := ubjval.GetByte()
			if err != nil {
				return err
			}
			v := reflect.ValueOf(b)
			field.Set(v)
		
		case kUBJArray:
			if fieldKind != reflect.Slice {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}
			arr, _ := ubjval.GetArray()
			slc := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(string(""))), len(arr), len(arr))
			for i, v := range arr {
				slc.Index(i).Set(reflect.ValueOf(v.value))
				field.Set(slc)
			}
		
		case kUBJObject:
			if fieldKind != reflect.Struct {
				return newUnmarshalError(key, fieldKind, ubjval.kind)
			}
			s := field.Interface()
			err := unmarshalValue(&s, ubjval)
			if err != nil {
				return err
			}
			field.Set(reflect.ValueOf(s))
		
		default:
			err := new(UnmarshalError)
			err.reason = fmt.Sprintf("Unmarshal encountered invalid type: %v", ubjval.kind)
			return err
	}
	
	rval.Set(copyVal)
	return nil;
}

func unmarshalValue(st interface{}, val *Value) error {
	obj, err := val.GetObject()
	if err != nil {
		return err
	}
	
	var unmarshalErr error = nil
	
	for key, item := range obj {
		unmarshalErr = unmarshalValueToStruct(key, item, st)
	}
	
	return unmarshalErr
}

func Unmarshal(st interface{}, data []byte) error {
	if m, ok := st.(Unmarshaler); ok {
		return m.UnmarshalUBJSON(data)
	}
	
	dval, err := Decode(data)
	if err != nil {
		return err
	}
	
	return unmarshalValue(st, dval)
}
